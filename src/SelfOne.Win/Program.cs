﻿using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Application = System.Windows.Forms.Application;

namespace SelfOne.Ui
{
    static class Program
    {
        static SAPbouiCOM.Application SBO_Application;
        static SAPbouiCOM.Form oForm;
        static Item oItem;

        static void Main()
        {
            SetApplication();
            StartApplication();
            
        }

        private static void StartApplication()
        {
            var decisionMessage = SBO_Application.MessageBox("Deseja usar versão híbrida?", 1, "Ok", "Cancelar", "");
            if (decisionMessage != decimal.Zero)
            {
                CreateMySimpleForm();
                SBO_Application.SetStatusBarMessage("Você está usando Addon Híbrido!", BoMessageTime.bmt_Short, false);
            }
        }

        private static void CreateMenuSap()
        {
            SAPbouiCOM.MenuItem oMenuItem = SBO_Application.Menus.Item("43520");

            Menus oMenus = SBO_Application.Menus;

            MenuCreationParams oMenuParams = ((MenuCreationParams)(SBO_Application.CreateObject(BoCreatableObjectType.cot_MenuCreationParams)));
            oMenuParams.Type = BoMenuType.mt_POPUP;
            oMenuParams.UniqueID = "444111";
            oMenuParams.String = "SAP Híbrido";
            oMenuParams.Position = oMenus.Count+1;

            oMenus = oMenuItem.SubMenus;

            try
            {
                oMenus.AddEx(oMenuParams);
                oMenuItem = SBO_Application.Menus.Item("444111");
                oMenus = oMenuItem.SubMenus;
                oMenuParams.Type = BoMenuType.mt_STRING;
                oMenuParams.UniqueID = "1244411";
                oMenuParams.String = "Abrir";
                oMenus.AddEx(oMenuParams);
            }
            catch (Exception ex)
            { 
                SBO_Application.MessageBox(ex.Message, 1, "Ok", "", "");
            }

            SBO_Application.MenuEvent += SBO_Application_MenuEvent;
            SBO_Application.AppEvent += SBO_Application_AppEvent;
        }

        private static void SBO_Application_AppEvent(BoAppEventTypes EventType)
        {
            SBO_Application.MessageBox("Evento criado com sucesso!", 1, "Ok", "", "");
        }

        private static void SBO_Application_MenuEvent(ref MenuEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            if (pVal.MenuUID == "1244411")
            {
                CreateMySimpleForm();
            }
        }

        private static void SetApplication()
        {
            SAPbouiCOM.SboGuiApi SboGuiApi = null;
            SboGuiApi = new SAPbouiCOM.SboGuiApi();
            SboGuiApi.Connect("0030002C0030002C00530041005000420044005F00440061007400650076002C0050004C006F006D0056004900490056");
            SBO_Application = SboGuiApi.GetApplication(-1);
        }


        private static void CreateMySimpleForm()
        {

            FormCreationParams oCreationParams = ((FormCreationParams)(SBO_Application.CreateObject(BoCreatableObjectType.cot_FormCreationParams)));

            oCreationParams.BorderStyle = BoFormBorderStyle.fbs_Sizable;
            oCreationParams.UniqueID = "SAPWEBBROWSER";

            oForm = SBO_Application.Forms.AddEx(oCreationParams);
            oForm.Title = "Addon Híbrido";
            oForm.Width = 1350;
            oForm.Height = 768;

            oItem = oForm.Items.Add("WebBrowser", BoFormItemTypes.it_WEB_BROWSER);
            oItem.Width = oForm.Width;
            oItem.Height = oForm.Height;

            oItem.AffectsFormMode = true;

            SAPbouiCOM.WebBrowser oWebBrowser = (SAPbouiCOM.WebBrowser)oItem.Specific;
            oWebBrowser.Url = "http://localhost:8080/";

            oForm.Visible = true;







        }
    }
}
