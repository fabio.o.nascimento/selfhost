﻿using Microsoft.Owin.FileSystems;
using Microsoft.Owin.StaticFiles;
using Owin;
using SelfOne.IDependency;
using SelfOne.SelfRest.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Unity;

namespace SelfOne.SelfRest
{
    class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.Use(async (context, next) =>
            {
                context.Response.Headers["Api"] = "Web Api Self Host";
                await next.Invoke();
            });




            app.Use(typeof(Middleware));

            var config = new HttpConfiguration();
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            app.UseWebApi(config);

            var options = new FileServerOptions
            {
                EnableDirectoryBrowsing = true,
                EnableDefaultFiles = true,
                DefaultFilesOptions = { DefaultFileNames = { "index.html" } },
                FileSystem = new PhysicalFileSystem("../../../www/"),
                StaticFileOptions = { ContentTypeProvider = new Provider() }
            };

            app.UseFileServer(options);




        }
    }
}
