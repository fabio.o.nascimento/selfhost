let Libs = [
    'One/controller/BaseController',
    'sap/ui/model/json/JSONModel',
    'sap/ui/Device',
    'One/model/formatter',
    'One/model/ExtendModel',
	'sap/m/MessageToast',	
];

sap.ui.define(Libs, function(BaseController, JSONModel, Device, formatter, ExtendModel, MessageToast){
    return BaseController.extend("One.controller.auth.Auth",{
        onInit:function(){
            this.Router = this.getRouter();
            oRouter = this.getRouter();
            oRouter.attachRoutePatternMatched(this.loadView, this);  
        },


        loadView:function(oEvent){
            console.log(oEvent.getParameter("name"));
            if (oEvent.getParameter("name") != "auth"){
                return;
            }
        },

        onLogin:function(){

            let User = this.byId("User"),
            loginButton = this.byId("loginButton"),
            Password = this.byId("Password");
            
            if(loginButton.getBusy()) 
                return;

            let ApiUrl = this.getApiUrl(this.api.securty);

            let AuthModel = new ExtendModel();

            loginButton.setBusy(true);

            AuthModel.insert(ApiUrl, 
            (sucess)=>{
                loginButton.setBusy(false);
                MessageToast.show(this.getText("auth.successMsg"));
            },
            (err)=>{
                loginButton.setBusy(false);
				this.getRouter().navTo("home");
                MessageToast.show(err.responseText);
            })

        },

    });
})