let Libs = [
    'One/controller/BaseController',
    'sap/ui/model/json/JSONModel',
    'sap/ui/Device',
    'One/model/formatter',
    'One/model/ExtendModel',
	'sap/m/MessageToast'	
];


sap.ui.define(Libs, function(BaseController, JSONModel, Device, formatter, ExtendModel, MessageToast){
    return BaseController.extend("One.controller.requestPurchase.List",{
        
        onInit:function(){
            this.loadRequests();
        },

        loadRequests:function(){
            let MockupModel = new JSONModel();
            
            MockupModel.setData({
                Requests:[{
                    Requester: "Gabriel",
                    Departament: "Compras",
                }]
            });

            this.setModel(MockupModel);

        },

        onPaginate:function(oEvent){
            MessageToast.show(this.getText("requestPurchase.Page") + oEvent.getSource().getPosition());
        },

        onFilterRequest:function(oEvent){
            MessageToast.show(oEvent.getSource().getSelectedKey());
        },
        onNewRequester:function(oEvent){
            this.getRouter().navTo("requestPurchaseEdit",{
                DocEntry:0
            });
        }

    });
})