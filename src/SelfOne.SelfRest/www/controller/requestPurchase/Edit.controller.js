let Libs = [
    'One/controller/BaseController',
    'sap/ui/model/json/JSONModel',
    'sap/ui/Device',
    'One/model/formatter',
    'One/model/ExtendModel',
	'sap/m/MessageToast'	
];


sap.ui.define(Libs, function(BaseController, JSONModel, Device, formatter, ExtendModel, MessageToast){
    return BaseController.extend("One.controller.requestPurchase,Edit",{
        
        onInit:function(){
            let oRouter;
            this.RequestModel = new ExtendModel();
            this.loadMeasurement();
            oRouter = this.getRouter();
            oRouter.attachRoutePatternMatched(this.loadView, this);  
        },

        loadMeasurement:function(){
            
            let Measurement = [
                {
                    "Name":"CX"
                },
                {
                    "Name":"UN"
                },
                {
                    "Name":"KG"
                },   
            ];
            let MeasurementModel = new JSONModel();
            MeasurementModel.setData(Measurement);
            this.setModel(MeasurementModel, "Measurement");
        },
        loadView:function(oEvent){

            let oEventData = oEvent.getParameter("arguments");
            if (oEvent.getParameter("name") != "requestPurchaseEdit"){
                return;
            }

            this.RequestModel.setData({ "Lines":[{}] });

            this.setModel(this.RequestModel);
            console.log(this.RequestModel.getData());
        },

        onAddNewLine:function(){
            let RequestModel = this.getModel();
            let Lines = RequestModel.getProperty("/Lines");
            Lines.push({});
            RequestModel.refresh(true);
        },
        
        onRemoveLine:function(oEvent){
            let TableLines = oEvent.getSource().getParent().getParent();
            
            let RequestModel = this.getModel();
            let Lines = RequestModel.getProperty("/Lines");
            
            TableLines.getSelectedIndices().map(line =>{
                Lines.splice(0, line);
            });

            RequestModel.refresh(true);


        },

    });
});