﻿using SelfOne.Domain.Projects;
using SelfOne.Infra;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SelfOne.Repository.Projects
{
    public class ProjectRepository : IProjectRepository
    {
        private AppDataContext _appDataContext;

        public ProjectRepository(AppDataContext dataContext)
        {
            _appDataContext = dataContext;
        }


        public List<Project> GetAll()
        {
            return _appDataContext.Project.ToList();
        }

        public Project GetByKey(int Id)
        {
            return _appDataContext.Project.Where(c => c.Id == Id).FirstOrDefault();
        }

        public void Add(Project project)
        {
            _appDataContext.Project.Add(project);
            _appDataContext.SaveChanges();
        }

        public void Remove(Project project)
        {
            _appDataContext.Entry(project).State = EntityState.Deleted;
            _appDataContext.SaveChanges();

        }

        public void Update(Project project)
        {
            _appDataContext.Entry(project).State = EntityState.Modified;
            _appDataContext.SaveChanges();
        }
    }
}
