﻿using SelfOne.Domain.Data;
using SelfOne.Infra;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Self.Infra.Data
{
    public class DataConnection : IDataConnection
    {
        private AppDataContext _context;

        public DataConnection(AppDataContext context)
        {
            _context = context;
        }
        public void Dispose()
        {
            _context.Dispose();
        }

        public IDbConnection GetDbConnection()
        {
            IDbConnection connection = null;
            connection = new SqlConnection("");
            connection.Open();
            return connection;
        }

    }
}
