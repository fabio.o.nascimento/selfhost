﻿using SelfOne.Domain.Projects;
using System.Data.Entity.ModelConfiguration;

namespace SelfOne.Infra
{
    internal class MapProject : EntityTypeConfiguration<Project>
    {
        public MapProject()
        {
            ToTable("Project")
            .Property(c => c.Id)
            .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)
            .IsRequired();
        }
    }
}