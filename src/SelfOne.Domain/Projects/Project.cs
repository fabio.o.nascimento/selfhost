﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SelfOne.Domain.Projects
{
    public class Project
    {
        public int Id { get; set; }
        public string PrjCode { get; set; }
        public string PrjName { get; set; }
        public string Active { get; set; }
        public string Description { get; set; }

        public void Validate()
        {
            throw new NotImplementedException();
        }
    }
}
